#include <QtWidgets>
#include <QtConcurrent>

using namespace QtConcurrent;

const int iterations = 200;

int spin(int &iteration)
{
    const int work = 1000 * 1000 * 40;
    volatile int v = iteration*10;
    for (int j = 0; j < work; ++j)
        ++v;

    qDebug() << "iteration" << iteration << "in thread" << QThread::currentThreadId();
    return v;
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    // Prepare the vector.
    QVector<int> vector;
    for (int i = 0; i < iterations; ++i)
        vector.append(i);

    // Create a progress dialog.
    QProgressDialog dialog;
    dialog.setLabelText(QString("Progressing using %1 thread(s)...").arg(QThread::idealThreadCount()));
    dialog.setRange(0,vector.size());

    // Display the dialog

    dialog.show();

    for(auto i=0; i<vector.size(); i++){
      vector[i] = spin(i);
      dialog.setValue(i);
    }
}
